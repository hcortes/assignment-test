const products = require('./helpers');
const yargs = require('yargs');

const argv = yargs.argv;

var source = argv._[1];
var provider = argv._[0];

console.log('Starting app...');

switch(provider) {
    case 'capterra':
        console.log('Importing capterra...');
        var objectFile = products.readFile(source);
        products.insertObjectDB(objectFile);
    break;
    case 'softwareadvice':
        console.log('Importing softwareadvice...');
        var objectFile = products.readFile(source);
        products.insertObjectDB(objectFile);
    break;
    /*case 'newprovider':
        console.log('Importing newprovider...');
        var objectFile = products.readURL(source);
        products.insertObjectDB(objectFile);
    break;*/
    default:
        console.log('Please enter a valid provider.');
}