const functions = require('./helpers');

//Expects a valid filename
test('Open Valid file', () => {
    expect(functions.readFile('feed-products/capterra.yaml')).toBeInstanceOf(Array);
});

//Expects an error when an invalid filename is submited
test('Expects an error when an invalid filename is submited', () => {
    expect(functions.readFile('error')).toBeInstanceOf(Error);
});

//Connects to database.
test('Connects to database and return true', () => {
    const object = [{Name: 'Test', Categories: 'Test', Twitter: '@test'}]
    expect(functions.insertObjectDB(object)).toBeTruthy();
});