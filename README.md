## Installation steps

To install the script, follow the instructions:

````
$ git clone https://hcortes@bitbucket.org/hcortes/assignment-test.git
$ cd assignment-test
/assignment-test$ npm install
````
Now you can run the script.

To import, start the script with the following command:

````
$ npm run import PROVIDER SOURCE-FILE
````

Currently there are 2 providers you can choose:

- capterra
- softwareadvice

example:

````
$ npm run import capterra feed-products/capterra.yaml
````

## Running unit test

To start the unit tests, type the following command:

````
$ npm test
````
The tests are done using Jest, a Javascript testing framework.