const yaml = require('yaml-js');
const fs = require('fs');
const path = require('path');

var products = []

var readFile = (filename) => {
    var fileextension = path.extname(filename);
    switch (fileextension) {
        case '.yaml':
            try {
                var yamlFile = yaml.load(fs.readFileSync(filename, 'utf8'))
                yamlFile.forEach(function(element){
                    var productElement = {}
                    productElement.Name = element.name
                    productElement.Categories = element.tags
                    productElement.Twitter = `@${element.twitter}`
                    products.push(productElement)
                });
                return products;
            } catch (e) {
                return e;
            }
        break;
        case '.json':
            try {
                var jsonFile = JSON.parse(fs.readFileSync(filename, 'utf8'));
                jsonFile.products.forEach(function(element){
                    var productElement = {}
                    productElement.Name = element.title
                    productElement.Categories = element.categories.join()
                    productElement.Twitter = element.twitter
                    products.push(productElement)
                });
                return products;
            } catch (e) {
                return e;
            }
        break;
        default:
            return new Error('Filename not recognized')
    }
}

var insertObjectDB = (products) => {
    console.log('Initializing database connection...');
    
    products.forEach(function(element){
        console.log('-----------------------------------');
        console.log('Importing: Name: ' + element.Name);
        console.log('Importing: Categories: ' + element.Categories);
        console.log('Importing: Twitter: ' + element.Twitter);
        console.log('-----------------------------------');
    });
    console.log('Closing database connection...');
    return true;
}

module.exports = {
    readFile,
    insertObjectDB
}